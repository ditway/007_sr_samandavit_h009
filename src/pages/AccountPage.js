import { Container } from "react-bootstrap";
import {
  Link,
  Route,
  Switch,
  useParams,
  useRouteMatch,
} from "react-router-dom";
import Header from "../components/Header";

export default function () {
  const { type } = useParams();
  const { url, path } = useRouteMatch();
  return (
    <div>
      <Header />
      <Container>
        <h2>Account</h2>
        <ul className="mb-3">
          <li>
            <Link to={`${url}/Netflix`}>Netflix</Link>
          </li>
          <li>
            <Link to={`${url}/Zillow Group`}>Zillow Group</Link>
          </li>
          <li>
            <Link to={`${url}/Yahoo`}>Yahoo</Link>
          </li>
          <li>
            <Link to={`${url}/Modius Create`}>Modius Create</Link>
          </li>
        </ul>
        <Switch>
          <Route path={`${path}/:type`}>
            <ID />
          </Route>
        </Switch>
      </Container>
    </div>
  );
}
function ID() {
  const { type } = useParams();
  return (
    <h4>
      ID: <span>{type}</span>
    </h4>
  );
}
