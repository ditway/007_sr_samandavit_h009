import Header from "../components/Header";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import Center from "../components/Center";
import { stories } from "./../data";
import StoryCard from "../components/StoryCard";
export default function () {
  const { id } = useParams();
  const [story, setStory] = useState();
  useEffect(() => {
    setTimeout(() => {
      setStory(stories.find((x) => x.id == id));
    }, 1000);
  }, [id]);
  return (
    <div>
      <Header />
      <Container>
        <Center className="mt-3">
          {!story ? <h5>Loading...</h5> : <StoryCard story={story} />}
        </Center>
      </Container>
    </div>
  );
}
