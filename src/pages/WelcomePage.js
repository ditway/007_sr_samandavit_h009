import { Button, Container } from "react-bootstrap";
import { Redirect } from "react-router";
import { useAuth } from "../components/AuthProvider";
import Center from "../components/Center";
import Header from "../components/Header";

export default function () {
  const { isAuth, loading, loggedOut } = useAuth();
  return (
    <div>
      <Header />
      <Container>
        <Center className="mt-3">
          {isAuth ? (
            <div>
              <h2 className="mb-3">Welcome</h2>
              <Button onClick={loggedOut} disabled={loading}>
                {loading ? "Logging Out..." : "Logout"}
              </Button>
            </div>
          ) : (
            <Redirect to="/auth" />
          )}
        </Center>
      </Container>
    </div>
  );
}
