import { Col, Row, Container } from "react-bootstrap";
import Header from "../components/Header";
import StoryCard from "../components/StoryCard";
import { stories } from "./../data";

export default function () {
  function renderCard() {
    return (
      <Row className="justify-content-center">
        {stories.map((item, index) => {
          return (
            <Col className="col-6 col-lg-3 col-md-4  p-2" key={index}>
              <StoryCard story={item} />
            </Col>
          );
        })}
      </Row>
    );
  }
  return (
    <div>
      <Header />
      <Container>{renderCard()}</Container>
    </div>
  );
}
