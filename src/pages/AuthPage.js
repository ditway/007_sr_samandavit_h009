import { Container } from "react-bootstrap";
import Center from "../components/Center";
import FormLogin from "../components/FormLogin";
import Header from "../components/Header";
import { useAuth } from "../components/AuthProvider";

export default function () {
  const { isAuth } = useAuth();
  return (
    <div>
      <Header />
      <Container>
        <Center className="mt-3">
          {isAuth ? <h2>Login Success</h2> : <FormLogin />}
        </Center>
      </Container>
    </div>
  );
}
