import { Button, ButtonGroup, Container } from "react-bootstrap";
import {
  Link,
  Route,
  Switch,
  useLocation,
  useRouteMatch,
} from "react-router-dom";
import Header from "../components/Header";

export default function () {
  const { url, path } = useRouteMatch();
  return (
    <div>
      <Header />
      <Container className="mt-3">
        <h3>Video</h3>
        <ButtonGroup className="mb-3">
          <Button variant="secondary" as={Link} to={`${url}/movie`}>
            Movie
          </Button>
          <Button variant="secondary" as={Link} to={`${url}/animation`}>
            Animation
          </Button>
        </ButtonGroup>
        <Switch>
          <Route path={`${path}/movie`}>
            <Movie />
          </Route>
          <Route path={`${path}/animation`}>
            <Animation />
          </Route>
        </Switch>
      </Container>
    </div>
  );
}
function VideoCategory(props) {
  const { types, title } = props;
  const { url } = useRouteMatch();
  const { search } = useLocation();
  const query = new URLSearchParams(search);
  return (
    <div>
      <h3>{title}</h3>
      <ButtonGroup className="mb-3">
        {types.map((item, index) => {
          return (
            <Button
              variant="secondary"
              key={index}
              as={Link}
              to={`${url}?type=${item}`}
            >
              {item}
            </Button>
          );
        })}
      </ButtonGroup>
      <h4>Please Choose Category: <span className="text-danger">{query.get("type")}</span></h4>
    </div>
  );
}
function Movie() {
  const types = ["Adventure", "Crime", "Action", "Romance", "Comedy"];
  return <VideoCategory title="Movie Category" types={types} />;
}
function Animation() {
  const types = ["Action", "Romance", "Comedy"];
  return <VideoCategory title="Animation Category" types={types} />;
}
