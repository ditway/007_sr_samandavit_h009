import AuthProvider from "./components/AuthProvider";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./pages/HomePage";
import VideoPage from "./pages/VideoPage";
import AccountPage from "./pages/AccountPage";
import WelcomePage from "./pages/WelcomePage";
import AuthPage from "./pages/AuthPage";

import "./App.css";
import DetailPage from "./pages/DetailPage";

function App() {
  return (
    <AuthProvider>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/video" component={VideoPage} />
          <Route path={["/account"]} component={AccountPage} />
          <Route path="/welcome" component={WelcomePage} />
          <Route path="/auth" component={AuthPage} />
          <Route path="/detail/:id" component={DetailPage} />
        </Switch>
      </BrowserRouter>
    </AuthProvider>
  );
}

export default App;
