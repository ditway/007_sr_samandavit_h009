import { Button, Card, Col, Form, Row } from "react-bootstrap";
import { useAuth } from "./AuthProvider";

export default function (props) {
  const { loggedIn, loading } = useAuth();
  function onSubmit(e) {
    e.preventDefault();
    loggedIn();
  }
  return (
    <Card style={{width: 450}}>
        <Card.Header>
           <h4>Login</h4>
        </Card.Header>
      <Card.Body>
        <Form onSubmit={onSubmit}>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" />
          </Form.Group>
          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" />
          </Form.Group>
          <Form.Group controlId="formBasicCheckbox">
            <Form.Check type="checkbox" label="Check me out" />
          </Form.Group>
          <Button disabled={loading} variant="primary" type="submit">
            {loading ? "Submitting..." : "Submit"}
          </Button>
        </Form>
      </Card.Body>
    </Card>
  );
}
