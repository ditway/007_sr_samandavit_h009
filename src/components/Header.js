import {
  Form,
  FormControl,
  Nav,
  Navbar,
  Button,
  Container,
} from "react-bootstrap";
import { NavLink } from "react-router-dom";
export default function () {
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand>React-Router</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbar-nav" />
        <Navbar.Collapse id="navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={NavLink} exact to="/">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/video">
              Video
            </Nav.Link>
            <Nav.Link as={NavLink} to="/account">
              Account
            </Nav.Link>
            <Nav.Link as={NavLink} to="/welcome">
              Welcome
            </Nav.Link>
            <Nav.Link as={NavLink} to="/auth">
              Auth
            </Nav.Link>
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
