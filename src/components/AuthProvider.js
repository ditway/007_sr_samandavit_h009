import { createContext, useContext, useState } from "react";

const AuthContext = createContext();

export function useAuth() {
  return useContext(AuthContext);
}

function useAuthProvider() {
  const [isAuth, setIsAuth] = useState();
  const [loading, setLoading] = useState();

  function loggedIn() {
    setLoading(true);
    setTimeout(() => {
      setIsAuth(true);
      setLoading(false);
    }, 1000);
  }
  function loggedOut() {
    setLoading(true);
    setTimeout(() => {
      setIsAuth(false);
      setLoading(false);
    }, 1000);
  }
  return {
    isAuth,
    loggedOut,
    loggedIn,
    loading,
  };
}
export default function (props) {
  const value = useAuthProvider();
  return (
    <AuthContext.Provider value={value}>{props.children}</AuthContext.Provider>
  );
}
