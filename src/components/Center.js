export default function (props) {
  return (
    <div className={`d-flex justify-content-center ${props.className}`}>
      {props.children}
    </div>
  );
}
